Потребность точно настроить отображения разных элементов страницы потребует от Вас одного — навыков html-вёрстки. Знание html, css и пакета Bottstrap-5 (с широким использованием которого создан веб-клиент ProtopiaEcosystem) сильно поможет Вам в исполнении самой сложной задачи создания любого отличного дизайна - последние штрихи. Чуть-чуть подвинуть, сделать поменьше или побольше, поменять цвет, скрыть ненужное и тп.

Здесь описываются инструменты использования css в модуле PE-Landing

## Cтилевые классы

Всё довольно просто - пишите через пробел классы, которые Вам нужно назначить. Такие модификаторы привинчены практически к любому типу элементов: Секции, контейнер содержимого, Карточки и отдельные поля Карточек.... Вот так выглядит модификатор css-класса:

![css-3.jpg](/assets/img/css-3.jpg)
*(1) Внешний вид модификатора css-классов*

Откуда брать имена классов? 

- [Bootstrap-5](https://getbootstrap.com/docs/5.1/getting-started/introduction/)
- назначаете свой уникальный класс и описываете его, как рассказано [здесь](#_1) 

## Кастомизация с использованием CSS

Вот так выглядит пустой модификатор CSS-аттрибутов в панелях:

![css-1-0](/assets/img/css-1-0.jpg)
*(2) Начальный вид модификатора css-аттрибутов*

Давайте рассмотрим подробнее как этим пользоваться.

![css-1](/assets/img/css-1.jpg)
*(3) Пример заполненного данными модификатора css-аттрибутов*

1. Обнулить изменения и привести модификатор в начальное положение (картинка 2)

2. Названия css-аттрибутов в нотации JavaScript. Она отличается от обычной тем, что все тире опускаются, а следующая за тире буква пишется заглавной.

3. Поле значений аттрибутов. Текстовые значения пишутся в строки. Аттрибуты численные - в модификатор чисел. Если аттрибут предполагает размерные значения, то модификатор значения превращается в вид, которым на рус.3 указан первый аттрибут (width) - можно мышкой или тапом выбрать числовые значения и единицы измерения (px, em, rem, %, vh, vw ). См. пункт (6)

4.  Кнопка добавления нового аттрибутами

5. Кнопка очистки значения аттрибутами

6. Если Вым неудобно пользоваться указательным набором значений, то кнопка переключает инпут в классический строковой.

7. Удалить аттрибут

8. Скопировать набор аттрибутов модификатора в буфер обмена 

9. Вставить из буфера обмена набор аттрибутов, который был скопирован (8) и полностью заменить существующий набор.


Некоторые типы аттрибутов позволяют вставлять интерактивные значения. Таковыми являются:

### Поле выбора цвета (*color, background, bodrerColor* ets.):
	
Внешний вид модификатора цвета:
	![css-2](/assets/img/css-2.jpg)
	
1. Выбораем цвет из Стилевого Шаблона, назначенного текущей Секции ([см. «Стилевые Шаблоны»](/landing/styled-templates/) ). В выпадающем списке выбираем ту модель цвета, которая будет зависеть от палитры Стилевого Шаблона.
		
2. Традиционный color-picker и назначение RGBA-значений
		
### Шрифт (FontFace)
	
Подробности [тут](#google-fonts)
	
	

## Расширение каскадной таблицы стилей

Как добавить или поправить уже существующие css-классы? Не вопрос!

1. Нажимаем кнопку «Редактировать страницу» (1)

![new_landing4](/assets/img/new_landing4.png)

2. Таб «Коррекция стилей»

3. В текстовое поле пишем классы с аттрибутами, как это делается в любом файле *.css

4. Сохраняем изменения. Перегружаем страницу. Вуаля.

## Google Fonts

### Шрифты, доступные всегда по-умолчанию.

Web-клиент ProtopiaEcosystem имеет 2 подключённых щрифта: [Open Sans](https://fonts.google.com/specimen/Open+Sans?query=Open+Sans) (для текстовых блоков) и [Yanone Kaffeesatz](https://fonts.google.com/specimen/Yanone+Kaffeesatz?query=Yanone+Kaffeesatz) (для заголовков). Эти шрифты Вы можете подключать в любом доступном для редактирования css-стилей [месте](#css) через css-директиву 
> FontFamily: Yanone Kaffeesatz

### Как подключить ещё 10 Google Fonts - шрифтов?

1. Открываем панель "Общие настройки страницы", нажав кнопку на панели редактирования страницы (1) 

	![new_landing4](/assets/img/new_landing4.png)

2. Идём в раздел "Шрифты"

3. Выбираем от 2 до 10 доступных шрифтов из библиотеки Google Fonts.

	3.1. Первый шрифт указан как заголовочный. 

	3.2. Вне зависимости от выбранного Стилевого шаблона, шрифт заголовков Секций, заголовков Карточек, заголовков элементов Таймлайнов и других заголовков будет этот. 

	3.3. Второй шрифт - шрифт подзаголовков

	3.4. Остальные шрифты доступны к подстановке посредством [инструментов редактирования css-стилей](#css) через css-директиву 
	> FontFamily: Yanone Kaffeesatz

!!! note 
	В целях ускорения загрузки Вашего сайта мы рекомендуем не использовать большое количество шрифтов. Это напрямую отражается на рейтинговании Вашего сайта поисковыми системами.

### Фильтрация шрифтов по языковым раскладкам
Для уменьшения выбора в выпадающих списках внизу страницы есть мульти-выбор языковых тегов Google Fonts.

По-умолчанию фильтруются только шрифты с кириллической раскладкой.