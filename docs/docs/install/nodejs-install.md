# Установка NodeJS-клиента 

`1.` Клонируйте git [отсюда](https://gitlab.com/genagl/node-startapp) на локальный компьютер

`2.` Установите базу данных [mongodb](https://www.mongodb.com/), создайте БД

`3.` Установите необходимые пакеты, выполнив в корневой директории команду:

```js
npm install
```

`4.` Сформируйте конфигурационные файлы командой

```js
npm run pe-config
```
`5.` Настройте сервер и установите модули командой

```js
npm run pe-install
```
	
`6.` запустите сервер командой:

```js
npm start
```
	
`7.` Доступ к консоли для тестирования команд:

`http://localhost:9095/graphql`
	
`8.` Скопируйте `server/config/client/config.json` в `src/config/config.json` react-клиента

`9.` Войдите как пользователь с email `admin@protopia-ecosystem.net` и паролем `admin`