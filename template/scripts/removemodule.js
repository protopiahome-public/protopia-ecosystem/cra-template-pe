const fs = require('fs');
const lt = require(`${__dirname}/../src/config/layouts.json`)
const child_process = require('child_process');

function remove_module( module_name )
{
    // clear layouts.json:schema
    const schema = {}
    Object.keys(lt['schema']).forEach(e =>
    {
        if( lt['schema'][e].module !== module_name)
        {
            schema[e] = ( lt['schema'][e] )
        }
    })
    lt['schema'] = schema

    // clear layouts.json:routing 
    const menus = ["profile", "extended_routes", "main_menu", "menu", "footer"]
    menus.map((group, i) =>
    {
        if( Array.isArray(lt.routing[group]) )
            lt.routing[group] = lt.routing[group].filter((route) => route.module !== module_name)
    })
    fs.writeFileSync( 
        `${__dirname}/../src/config/layouts.json`, 
        JSON.stringify(lt, null, 2 ) 
    );

    // clear layouts.json:widget-area by module_name
    const wa = {}
    Object.keys(lt['widget-area']).forEach(e =>
    {
        if( lt['widget-area'][e].module !== module_name)
        {
            wa[e] = ( lt['widget-area'][e] )
        }
    })
    lt['widget-area'] = wa

    // clear layouts.json:widgets by module_name
    const widgets = {}
    Object.keys(lt['widgets']).forEach(e =>
    {
        if( lt['widgets'][e].module !== module_name)
        {
            widgets[e] = ( lt['widgets'][e] )
        }
    })
    lt['widgets'] = widgets

    // clear layouts.json:extentions by module_name    
    const extentions = {}
    Object.keys(lt['extentions']).forEach(e =>
    {
        if( lt['extentions'][e].module !== module_name)
        {
            extentions[e] = ( lt['extentions'][e] )
        }
    })
    lt['extentions'] = extentions

    // clear layouts.json:views by module_name      
    const views = {}
    Object.keys(lt['views']).forEach(e =>
    {
        if( lt['views'][e].module !== module_name)
        {
            views[e] = ( lt['views'][e] )
        }
    })
    lt['views'] = views

    // clear layouts.json:modules by module_name
    delete lt.modules[ module_name ]

    // save layouts.json
    fs.writeFileSync( 
        `${__dirname}/../src/config/layouts.json`, 
        JSON.stringify(lt, null, 2 ) 
    );

    // remove module folder from src/modules    
    fs.rmSync(`${__dirname}/../src/modules/${module_name}`, { recursive: true, force: true })

    // remove module source from external
    fs.rmSync(`${__dirname}/../external/${module_name}`, { recursive: true, force: true })


}

const args =  process.argv;
//console.log(`${args[3]}`.substring(0, 19), "https://gitlab.com/")
if(args[2])
{
    //const module_name = `${args[2]}`
    //const splits = `${args[3]}`.split("/")
    //console.log( splits[splits.length - 1] )  
    //console.log( lt.modules[module_name] )  
    remove_module( `${args[2]}` ) 
}
else
{
    console.error("Укажите название плагина и его местонахождение на https//:gitlab.com, который надо установить")
}