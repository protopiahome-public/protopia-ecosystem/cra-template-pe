const fs = require('fs');
const lt = require(`${__dirname}/../src/config/layouts.json`);

let module_title = ""
const args =  process.argv;
if(args[2])
{
    var name = args[2];
    module_title = args[3] ? args[3] : "";
    let is = true
    if( lt.modules[name]) 
    {
        console.error("this module exists") 
        is = false
    }
    if( fs.existsSync(`${__dirname}/../src/modules${name}`) )
    {
        console.error("this module's dir exists")
        is = false
    }
    if( name.lastIndexOf("-module") === -1 )
    {
        console.error("name must finished like '-module'")
        is = false
    }
    if( name.indexOf("pe-") === -1 )
    {
        console.error("name must start like 'pe-'")
        is = false
    }
    if(is)
    {
        start_draw(name)
    }    
}
else
{
    console.error("Insert name of new module")
}
function start_draw(module_name)
{
    const module_dir = `${__dirname}/../src/modules/${module_name}`

    fs.mkdirSync(module_dir)
    fs.mkdirSync(`${module_dir}/assets`)
    fs.mkdirSync(`${module_dir}/assets/img`)
    fs.mkdirSync(`${module_dir}/assets/css`)
    fs.mkdirSync(`${module_dir}/public`)
    fs.mkdirSync(`${module_dir}/public/assets`)
    fs.mkdirSync(`${module_dir}/public/assets/css`)
    fs.mkdirSync(`${module_dir}/public/assets/img`)
    fs.mkdirSync(`${module_dir}/public/assets/js`)
    fs.mkdirSync(`${module_dir}/views`)
    fs.mkdirSync(`${module_dir}/widgets`)
    fs.mkdirSync(`${module_dir}/data`)
    fs.mkdirSync(`${module_dir}/components`)
    fs.mkdirSync(`${module_dir}/lng`)
    console.log("Directories created successfully")

    //
    fs.writeFileSync( 
        `${module_dir}/lng/en-EN.json`, 
        JSON.stringify({}, null, 2 ) 
    );
    fs.writeFileSync( 
        `${module_dir}/lng/ru-RU.json`, 
        JSON.stringify({}, null, 2 ) 
    );
    console.log("Dictionary created successfully")

    // 
    let lt = {
        commentary: {},
        id: module_name,
        title: module_title,
        description:"", 
        preority: 1,
        version: "0.1.0",
        author: "",
        documentation:"",
        git:"",
        data: {},
        components:{},
        views : {},
        widgets : {},
        extentions: {},
        schema: {},
        routing: {}
    }
    lt["widget-area"]   = {}
    fs.writeFileSync( 
        `${module_dir}/layouts.json`, 
        JSON.stringify(lt, null, 2 ) 
    );
    console.log("Layout scenario created successfully")

    //
    let package = {
        dependencies : {}
    }
    fs.writeFileSync( 
        `${module_dir}/package.json`, 
        JSON.stringify(package, null, 2 ) 
    );
    console.log("Package.json created successfully")

}