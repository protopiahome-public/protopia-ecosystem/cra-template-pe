const fs = require("fs")
const child_process = require("child_process")
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// child_process.execSync(`npm i --save react-pe-utilities`)
// child_process.execSync(`npm i --save react-pe-units`)
// child_process.execSync(`npm i --save react-pe-scalars`)
// child_process.execSync(`npm i --save react-pe-basic-view`)
// child_process.execSync(`npm i --save react-pe-layouts`)
// child_process.execSync(`npm i --save react-pe-layout-app`)

// install default mpdules
child_process.execSync(`npm run add-module pe-basic-module https://gitlab.com/protopiahome-public/protopia-ecosystem/react-pe-basic-module`)
child_process.execSync(`npm run add-module pe-admin-module https://gitlab.com/protopiahome-public/protopia-ecosystem/react-pe-admin-module`)

// create config
rl.question('put your server url', function (server_url) {
  rl.question(`put your app's JWT-token`, function (assertion_token) {
    rl.question(`put "http" or "https" protocol of your client`, function (link_type) {
      rl.question(`put your client url`, function (app_url) {
        let config = {
          server_url,
          assertion_token,
          link_type,
          app_url,
          yandex_map_api_key:"",
          gitlab_private_token : "f4FBDvKV-BzHMpFP75jk"
        }
        fs.writeFileSync( 
          `${__dirname}/../src/config/config.json`, 
          JSON.stringify(config, null, 2 ) 
        );
        rl.close();
      });
    });
  });
});
rl.on('close', function () {
  console.log('\nSuccess. Type "npm start" to continue and enjoy.');
  process.exit(0);
});

