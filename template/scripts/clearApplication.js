const fs = require('fs');
const lt = require(`${__dirname}/../src/config/layouts.json`)
const child_process = require('child_process');
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function clearApplication()
{
    const modules = lt.modules;
    Object.keys(modules).forEach(module => {
        child_process.execSync(`npm run remove-module ${module}`);
    })
}
rl.question('Are you want clear all modules and libraries? Aswer Y or N.', function (answer) {
    if(answer == "y")
    {
        console.log("Start clear all modules and libraries.");
        clearApplication();        
        console.log("Application empty.");
    }
})