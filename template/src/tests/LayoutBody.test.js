import LayoutBody from "states/LayoutBody"
import Layouts from "react-pe-layouts"

describe( "загрузка конфигурации", () => {
    //let mockValue = Layouts().app.init_method
    beforeAll(() => {
        // загруюаем данные конфигурации загрузки 
    })
    test("файл конфигурации layouts.json", () =>
    {
        expect(Layouts()).not.toBeNull()
        expect(Layouts().app).not.toBeNull()
        expect(Layouts().template).not.toBeNull()
    })
    afterEach(() =>
    {
        jest.clearAllMocks()
    })
})